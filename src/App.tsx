import 'react-native-gesture-handler';
import * as React from 'react';
import {
  NavigationContainer,
  NavigationContainerRef,
} from '@react-navigation/native';
import {MainStackNavigator} from './navigation/MainStackNavigator';

export const navigationRef = React.createRef<NavigationContainerRef>();

const App = () => {
  return (
    <NavigationContainer ref={navigationRef}>
      <MainStackNavigator />
    </NavigationContainer>
  );
};

export default App;
