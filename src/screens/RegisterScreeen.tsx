import React from 'react';
import {CustomActionButton} from '../components/CustomActionButton';
import {Text, View, ImageBackground, TextInput} from 'react-native';
import {CustomTextFiled} from '../components/CustomTextFiled';
import {CustomTextArea} from '../components/CustomTextArea';
import {navigate} from '../navigation/MainStackNavigator';
import { ForgotPasswordButton } from '../components/ForgotPasswordButton';
import { AccountButton } from '../components/AccountButton';


export const RegisterScreeen = () => {
  return (
    <>
      <ImageBackground style={{ width: '100%', height: '100%', flex: 1 }}
        resizeMode='cover'
        source={require('../assets/Login_BG.png')}>

        <View style={{ paddingTop: 20}}>
          <Text style={{ textAlign: 'center',fontWeight: 'bold',fontStyle: 'normal',fontSize: 30,color : 'white'}}>REGISTER</Text>
        </View>

        <View style={{ paddingTop: 20, paddingLeft: 30, paddingRight: 30 }}>
          <CustomTextFiled name="First Name" isPasswordField={false} ></CustomTextFiled>
        </View>
        <View style={{ paddingTop: 5, paddingLeft: 30, paddingRight: 30 }}>
          <CustomTextFiled name="Last Name" isPasswordField={false} ></CustomTextFiled>
        </View>

        <View style={{ paddingTop: 5, paddingLeft: 30, paddingRight: 30 }}>
          <CustomTextFiled name="Mobile" isPasswordField={false} ></CustomTextFiled>
        </View>


        <View style={{ paddingTop: 5, paddingLeft: 30, paddingRight: 30 }}>
          <CustomTextFiled name="Email" isPasswordField={false} ></CustomTextFiled>
        </View>

        <View style={{ paddingTop: 5, paddingLeft: 30, paddingRight: 30 }}>
          <CustomTextFiled name="Birthday" isPasswordField={false} ></CustomTextFiled>
        </View>

        <View style={{paddingTop: 5, paddingLeft: 30, paddingRight: 30 }}>
          <CustomTextArea name="Address" isPasswordField={false} ></CustomTextArea>
        </View>

        <View style={{ paddingTop: 20, paddingLeft: 30, paddingRight: 30 }}>
          <CustomTextFiled name="Region" isPasswordField={false} ></CustomTextFiled>
        </View>

        <View style={{ paddingTop: 5, paddingLeft: 30, paddingRight: 30 }}>
          <CustomTextFiled name="City" isPasswordField={false} ></CustomTextFiled>
        </View>

      <CustomActionButton 
          onPress={() => {
            navigate('Login');
          }}>
        </CustomActionButton>

      </ImageBackground>

    </>
  );
  };
