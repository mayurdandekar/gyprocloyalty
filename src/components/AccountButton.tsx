import React from 'react';
import {
  TouchableOpacityProps, Text, View,
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { commonStyles } from '../styles/commonStyles';

interface props {
  onPress: () => void;
}

export const AccountButton = (
  props: React.PropsWithChildren<TouchableOpacityProps> & props,
) => {
  return (

    <View style={{position: 'absolute', bottom: 20, margin: 10,right: 10,}}>
    <TouchableOpacity onPress={props.onPress}>
    {props.children}
    <Text style = {{textAlign: 'left',color : 'red',fontSize : 15}}>
            Don't have account?
            <Text style = {{textAlign: 'left',color : 'blue',fontSize : 15}}>
                Register
            </Text>
    </Text>
  </TouchableOpacity>
    </View>
  );
};
