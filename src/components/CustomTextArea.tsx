import React from 'react';
import { commonStyles } from '../styles/commonStyles';
import {
    View,
    TextInput, KeyboardTypeOptions,StyleSheet
} from 'react-native';


interface Props {
    name?: string;
    value?: string;
    isPasswordField?: boolean;
    keyboardType?: KeyboardTypeOptions;
    //onChangeText: (value: string) => void;
}

export const CustomTextArea = (props: React.PropsWithChildren<Props>) => {

    const placeHolderName = props.name;
    const isPasswordFiled = props.isPasswordField;

    return (
            <TextInput
                style={styles.textArea}
                placeholder= {placeHolderName}
                numberOfLines={10}
                multiline={true}
                //placeholderTextColor="grey"
            />
    );

    
};

const styles = StyleSheet.create({
    textArea: {
      height: 80,
      justifyContent: "flex-start",
      fontSize: 18,
      paddingLeft: 10,
      paddingRight : 10,
      backgroundColor : 'white',
      borderRadius: 20,

    }
  })


