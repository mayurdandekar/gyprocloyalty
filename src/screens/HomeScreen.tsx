import {CustomButton} from '../components/CustomButton';
import React from 'react';
import {Text, View} from 'react-native';
import {navigate} from '../navigation/MainStackNavigator';

export const HomeScreen = () => {
  return (
    <>
      <View>
        <CustomButton
          onPress={() => {
            navigate('BarcodeScanScreen');
          }}>
          <Text>Navigate to Barcode Scanner</Text>
        </CustomButton>
      </View>
    </>
  );
};
