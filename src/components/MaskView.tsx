import React from 'react';
import {StyleSheet, View} from 'react-native';
import {commonStyles} from '../styles/commonStyles';

interface Props {
  borderWidth?: number;
  size?: number;
  marginTop?: number;
  marginBottom?: number;
  color?: string;
  left?: number;
  right?: number;
}

export const MaskView = (props: React.PropsWithChildren<Props>) => {
  const borderWidth = props.borderWidth || 2;
  const color = props.color || '#fff';
  const size = props.size || 32;
  const marginTop = props.marginTop || 44;
  const marginBottom = props.marginBottom || 44;
  const left = props.left || 24;
  const right = props.left || 24;

  return (
    <View
      style={[
        commonStyles.center,
        StyleSheet.absoluteFillObject,
        {
          left: left,
          right: right,
          top: marginTop,
          bottom: marginBottom,
        },
      ]}>
      <View
        style={[
          styles.common,
          {
            width: size,
            height: size,
            top: 0,
            left: 0,
            borderRightColor: 'transparent',
            borderBottomColor: 'transparent',
            borderLeftColor: color,
            borderTopColor: color,
            borderWidth,
          },
        ]}
      />
      <View
        style={[
          styles.common,
          {
            width: size,
            height: size,
            top: 0,
            right: 0,
            borderLeftColor: 'transparent',
            borderBottomColor: 'transparent',
            borderRightColor: color,
            borderTopColor: color,
            borderWidth,
          },
        ]}
      />
      <View
        style={[
          styles.common,
          {
            width: size,
            height: size,
            right: 0,
            bottom: 0,
            borderRightColor: color,
            borderBottomColor: color,
            borderLeftColor: 'transparent',
            borderTopColor: 'transparent',
            borderWidth,
          },
        ]}
      />
      <View
        style={[
          styles.common,
          {
            width: size,
            height: size,
            left: 0,
            bottom: 0,
            borderRightColor: 'transparent',
            borderBottomColor: color,
            borderLeftColor: color,
            borderTopColor: 'transparent',
            borderWidth,
          },
        ]}
      />
      {props.children}
    </View>
  );
};

const styles = StyleSheet.create({
  common: {
    backgroundColor: 'transparent',
    position: 'absolute',
    borderWidth: 2,
    shadowColor: '#00000080',
  },
});
