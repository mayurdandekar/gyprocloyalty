import React from 'react';
import { CustomActionButton } from '../components/CustomActionButton';
import { Text, View, ImageBackground } from 'react-native';
import { CustomTextFiled } from '../components/CustomTextFiled';
import { ForgotPasswordButton } from '../components/ForgotPasswordButton';
import { AccountButton } from '../components/AccountButton';


import { navigate } from '../navigation/MainStackNavigator';

//import {backImage} from '../assets/Login_BG.png'


const image = { uri: "https://reactjs.org/logo-og.png" };


export const LoginScreen = () => {
  return (
    <>
      <ImageBackground style={{ width: '100%', height: '100%', flex: 1 }}
        resizeMode='cover'
        source={require('../assets/Login_BG.png')}>

        <View style={{ paddingTop: 40}}>
          <Text style={{ textAlign: 'center',fontWeight: 'bold',fontStyle: 'normal',fontSize: 30,color : 'white'}}>LOGIN</Text>
        </View>

        <View style={{ paddingTop: 40, paddingLeft: 30, paddingRight: 30 }}>
          <CustomTextFiled name="Name" isPasswordField={false} ></CustomTextFiled>
        </View>
        <View style={{ paddingTop: 10, paddingLeft: 30, paddingRight: 30 }}>
          <CustomTextFiled name="Password" isPasswordField={true} ></CustomTextFiled>
        </View>

        <ForgotPasswordButton 
          onPress={() => {
            navigate('Login');
          }}>
        </ForgotPasswordButton>
     
      <CustomActionButton 
          onPress={() => {
            navigate('Login');
          }}>
        </CustomActionButton>

        <AccountButton 
          onPress={() => {
            navigate('Login');
          }}>
        </AccountButton>
      
      </ImageBackground>

    </>
  );
};




