import React from 'react';
import {
  Platform,
  TouchableHighlight,
  TouchableNativeFeedback,
  TouchableNativeFeedbackProps,
  View,
  StyleSheet,
  TouchableOpacityProps, Image,
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { commonStyles } from '../styles/commonStyles';

interface props {
  onPress: () => void;
}

export const CustomActionButton = (
  props: React.PropsWithChildren<TouchableOpacityProps> & props,
) => {
  return (
    <View style={{position: 'absolute', bottom: 50, margin: 10,right: 10,}}>
      <TouchableOpacity onPress={props.onPress} style={commonStyles.CustomActionButton}>
        {props.children}
        <Image
          source={require('../assets/Submit-button.png')}
          style={commonStyles.imageSubmitButton}
        />
      </TouchableOpacity>
    </View>
  );
};
