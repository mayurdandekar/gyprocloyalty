import React from 'react';
import {ViewStyle} from 'react-native';
import Svg, {Defs, G, Path} from 'react-native-svg';

interface Props {
  color?: string;
  height?: string;
  width?: string;
  opacity?: string;
  style?: ViewStyle;
}
export const BarCodeScannerIcon = (props: Props) => {
  const defaultWidth = '298.061';
  const defaultHeight = '201.061';
  const fillColor = props.color || '#fff';
  const defaultOpacity = '0.19';

  return (
    <Svg
      width={props.width || defaultWidth}
      height={props.height || defaultHeight}
      viewBox="0 0 298.061 201.061"
      style={props.style}
      {...props}>
      <Defs></Defs>
      <G fill={fillColor}>
        <Path
          id="prefix__b"
          d="M34.727 52.601h1.568v59.814h-1.568zM40.999 52.601h1.568v59.814h-1.568zM44.135 52.601h4.704v59.814h-4.704zM50.407 52.601h4.704v59.814h-4.704zM56.678 52.601h1.568v59.814h-1.568zM59.814 52.601h4.704v59.814h-4.704zM66.086 52.601h1.568v59.814h-1.568zM72.358 52.601h1.568v59.814h-1.568zM75.494 52.601h1.568v59.814h-1.568zM78.63 52.601h4.704v59.814H78.63zM84.902 52.601h1.568v59.814h-1.568zM88.038 52.601h4.704v59.814h-4.704zM97.445 52.601h1.568v59.814h-1.568zM100.581 52.601h1.568v59.814h-1.568zM103.717 52.601h4.704v59.814h-4.704zM109.989 52.601h4.704v59.814h-4.704zM116.261 52.601h4.704v59.814h-4.704zM125.668 52.601h1.568v59.814h-1.568zM128.805 52.601h1.568v59.814h-1.568zM131.94 52.601h1.568v59.814h-1.568zM135.076 52.601h1.568v59.814h-1.568zM138.212 52.601h1.568v59.814h-1.568zM144.484 52.601h4.704v59.814h-4.704zM150.756 52.601h1.568v59.814h-1.568zM153.892 52.601h4.704v59.814h-4.704zM160.164 52.601h4.704v59.814h-4.704zM166.436 52.601h1.568v59.814h-1.568zM172.707 52.601h4.704v59.814h-4.704zM178.979 52.601h1.568v59.814h-1.568zM182.115 52.601h1.568v59.814h-1.568zM185.251 52.601h1.568v59.814h-1.568zM188.387 52.601h4.704v59.814h-4.704zM197.795 52.601h4.704v59.814h-4.704zM204.066 52.601h1.568v59.814h-1.568zM207.203 52.601h1.568v59.814h-1.568zM210.338 52.601h1.568v59.814h-1.568zM213.474 52.601h1.568v59.814h-1.568zM219.746 52.601h1.568v59.814h-1.568zM222.882 52.601h4.704v59.814h-4.704zM229.154 52.601h4.704v59.814h-4.704zM235.426 52.601h1.568v59.814h-1.568zM241.698 52.601h1.568v59.814h-1.568zM244.833 52.601h4.704v59.814h-4.704zM251.105 52.601h4.704v59.814h-4.704zM257.377 52.601h1.568v59.814h-1.568z"
        />
      </G>
      <G opacity={props.opacity || defaultOpacity}>
        <Path
          id="prefix__b"
          d="M1 166v-24h2v22h22v2zM293 165h-24v-2h22v-22h2zM293 0v24h-2V2h-22V0zM0 1h24v2H2v22H0z"
          fill={fillColor}
          opacity={props.opacity}
        />
      </G>
      <Path
        fill="none"
        stroke="#ed6e52"
        strokeWidth={2}
        opacity={0.632}
        d="M1 80.79l293 1"
      />
    </Svg>
  );
};
