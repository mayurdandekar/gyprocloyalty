import React, {useRef, useState} from 'react';
import {Barcode, RNCamera} from 'react-native-camera';
import {Platform, StyleSheet, ViewStyle} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';

export type DidScanBarCode = (data: string, type: string) => void;

interface Props {
  didScanBarCode: DidScanBarCode;
  scanBarCode: boolean;
  style?: ViewStyle;
}

export const ScanBarCode = (props: React.PropsWithChildren<Props>) => {
  const cameraRef = useRef<RNCamera>(null);
  const {didScanBarCode, scanBarCode} = props;
  const [isScannerVisible, setScannerVisible] = useState(false);
  const barcodeTypes = [RNCamera.Constants.BarCodeType.code39];

  useFocusEffect(
    React.useCallback(() => {
      setScannerVisible(true);

      return () => {
        setScannerVisible(false);
      };
    }, []),
  );

  const onBarCodeRead = React.useCallback(
    (e: {data: string; type: string}) => {
      if (!scanBarCode) {
        return;
      }
      didScanBarCode(e.data, e.type);
    },
    [didScanBarCode, scanBarCode],
  );

  const onGoogleVisionBarCodeRead = React.useCallback(
    (event: {barcodes: Barcode[]}) => {
      if (event?.barcodes?.length < 1) {
        return;
      }
      onBarCodeRead({
        data: event.barcodes[0].data,
        type: event.barcodes[0].type,
      });
    },
    [onBarCodeRead],
  );

  return (
    <>
      {isScannerVisible && (
        <RNCamera
          ref={cameraRef}
          style={[styles.preview, props.style]}
          autoFocusPointOfInterest={{x: 0.5, y: 0.5}}
          zoom={Platform.OS === 'android' ? 0.2 : undefined}
          captureAudio={false}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.on}
          androidCameraPermissionOptions={{
            title: 'Camera permission',
            message: 'Please allow to access camera ',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          // rectOfInterest={{
          //     x: 0.7,
          //     y: 0.2,
          //     width: 0.5,
          //     height: 0.001,
          // }}
          // cameraViewDimensions={{
          //     width: 0.001,
          //     height: 0.001,
          // }}
          onBarCodeRead={onBarCodeRead}
          barCodeTypes={barcodeTypes}>
          {props.children}
        </RNCamera>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  preview: {
    flex: 1,
  },
});
