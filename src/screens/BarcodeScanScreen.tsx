import {navigate} from '../navigation/MainStackNavigator';
import * as React from 'react';
import {useState} from 'react';
import {View, SafeAreaView, StatusBar, Text} from 'react-native';
import {BarCodeScanInfoPopup} from '../components/BarCodeScanInfoPopup';
import {MaskView} from '../components/MaskView';
import {ScanBarCode} from '../components/ScanBarCode';
import {commonStyles} from '../styles/commonStyles';

export const BarcodeScanScreen = () => {
  const [scanBarCode, setScanBarCode] = useState(true);

  const handleDidSelectBarCodeType = async (data: string, type: string) => {
    setScanBarCode(false);
    console.log(data, type);
    setScanBarCode(true);
    navigate('HomeScreen');
  };

  return (
    <>
      <StatusBar />
      <SafeAreaView style={{flex: 1}}>
        <ScanBarCode
          scanBarCode={scanBarCode}
          didScanBarCode={handleDidSelectBarCodeType}>
          <>
            <MaskView marginTop={190}>
              <View
                style={{
                  backgroundColor: '#ED6E52',
                  height: 4,
                  width: '100%',
                }}
              />
            </MaskView>
          </>
        </ScanBarCode>
        <View
          style={[
            commonStyles.center,
            {
              flex: 0.5,
              marginBottom: 0,
              marginTop: 130,
            },
          ]}>
          <BarCodeScanInfoPopup />
          <View style={{marginBottom: 20}}>
            <Text>{'Enter Code manually'}</Text>
          </View>
        </View>
      </SafeAreaView>
    </>
  );
};
