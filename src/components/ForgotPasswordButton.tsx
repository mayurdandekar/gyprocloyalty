import React from 'react';
import {
    TouchableOpacityProps, Text,
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { commonStyles } from '../styles/commonStyles';

interface props {
  onPress: () => void;
}

export const ForgotPasswordButton = (
  props: React.PropsWithChildren<TouchableOpacityProps> & props,
) => {
  return (
      <TouchableOpacity onPress={props.onPress} style={commonStyles.ForogotPaswordButton} >
        {props.children}
        <Text style = {{textAlign: 'left',color : 'white',fontSize : 18}}>
                Forogot password ?
        </Text>
      </TouchableOpacity>
  );
};