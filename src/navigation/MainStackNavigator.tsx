import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import {HomeScreen} from '../screens/HomeScreen';
import {BarcodeScanScreen} from '../screens/BarcodeScanScreen';
import {navigationRef} from '../App';
import { Image } from 'react-native';
import {LoginScreen} from '../screens/LoginScreen'

const DefaultStack = createStackNavigator();
const Drawer = createDrawerNavigator();

export const MainStackNavigator = () => {
  return (
    <DefaultStack.Navigator>
      <DefaultStack.Screen name={'HomeScreen'} component={DrawerNavigator} />
      <DefaultStack.Screen
        name={'BarcodeScanScreen'}
        component={DrawerNavigator}
      />
    </DefaultStack.Navigator>
  );
};

export const DrawerNavigator= () =>  {
  return (
    
      <Drawer.Navigator 
      drawerStyle={{
        backgroundColor: '#ffffff',
        width: 240,
      }}
      initialRouteName="HomeScreen">
        <Drawer.Screen name="HomeScreenn" component={HomeScreen} 
        options={{ drawerLabel: 'User ID', drawerIcon: ({ color }) => (
          <Image
            source={require("../assets/icons/home.png")}
            resizeMode="contain"
            style={{ width: 60, height: 60}}
          />
        ) }}
        />
        <Drawer.Screen name="HomeScreen" component={HomeScreen} 
        options={{ drawerLabel: 'Home', drawerIcon: ({ color }) => (
          <Image
            source={require("../assets/icons/home.png")}
            resizeMode="contain"
            style={{ width: 30, height: 30}}
          />
        ) }}
        />
        <Drawer.Screen name="LoginScreen" component={LoginScreen} 
        options={{ drawerLabel: 'LoginScreen', drawerIcon: ({ color }) => (
          <Image
            source={require("../assets/icons/points_history.png")}
            resizeMode="contain"
            style={{ width: 30, height: 30}}
          />
        ) }}
        />
        <Drawer.Screen name="Reedem History" component={HomeScreen} 
        options={{ drawerLabel: 'Reedem History', drawerIcon: ({ color }) => (
          <Image
            source={require("../assets/icons/redeem_history.png")}
            resizeMode="contain"
            style={{ width: 30, height: 30}}
          />
        ) }}
        />
        <Drawer.Screen name="Customer Service" component={HomeScreen} 
        options={{ drawerLabel: 'Customer Service', drawerIcon: ({ color }) => (
          <Image
            source={require("../assets/icons/customer_support.png")}
            resizeMode="contain"
            style={{ width: 30, height: 30}}
          />
        ) }}
        />
        <Drawer.Screen name="Setting" component={HomeScreen} 
        options={{ drawerLabel: 'Setting', drawerIcon: ({ color }) => (
          <Image
            source={require("../assets/icons/settings.png")}
            resizeMode="contain"
            style={{ width: 30, height: 30}}
          />
        ) }}
        />
        <Drawer.Screen name="About Us" component={HomeScreen} 
        options={{ drawerLabel: 'About Us', drawerIcon: ({ color }) => (
          <Image
            source={require("../assets/icons/about.png")}
            resizeMode="contain"
            style={{ width: 30, height: 30}}
          />
        ) }}
        />
      </Drawer.Navigator>
    
  );
};

export const navigate = (screenName: string) => {
  navigationRef.current?.navigate(screenName);
};
