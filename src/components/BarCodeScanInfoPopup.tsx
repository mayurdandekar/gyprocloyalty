import React, {useContext} from 'react';
import {BarCodeScannerIcon} from '../assets/BarCodeScannerIcon';
import {Text} from 'react-native';

export const BarCodeScanInfoPopup = () => {
  return (
    <>
      <BarCodeScannerIcon
        color="#000"
        width="101"
        height="61"
        opacity="10.19"
      />
      <Text
        style={{
          alignSelf: 'center',
          flexWrap: 'wrap',
          flex: 1,
        }}>
        {'Hold RedBar Above BarCode'}
      </Text>
    </>
  );
};
