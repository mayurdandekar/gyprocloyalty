import { StyleSheet } from 'react-native';

export const commonStyles = StyleSheet.create({
  customButton: {
    height: 60,
    backgroundColor: 'lightblue',
    justifyContent: 'center',
    alignItems: 'center',
  },

  center: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  marginHorizontal: { marginHorizontal: 20 },

  customTextFiled: {
    paddingLeft: 10,
    paddingRight : 10,
    height: 40,
    width: "100%",
    borderRadius: 20,
    marginBottom: 20,
    fontSize: 18,
    backgroundColor : 'white'
  },

  imageSubmitButton :{
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    resizeMode : 'cover',
  },

  CustomActionButton: {
    width:200,height :60
  },

  ForogotPaswordButton : {
    width:'42%',
    height :'20%',
    left: 200,
    top: 10,
  },

  AccountButton : {

    position: 'absolute', 
    bottom: 0, 
    margin: 10,
    right: 40,
    backgroundColor : 'red'
  }
  
});
